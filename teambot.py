#!/usr/bin/python3

import configparser
import html
import os
import requests
import sqlite3
import traceback
import urllib.parse
from datetime import datetime,timezone

config_dir = os.getenv("XDG_CONFIG_HOME", os.path.expanduser("~/.config"))
config_file = config_dir + "/teambotrc"

config = configparser.ConfigParser()
config.read(config_file)

host = config.get("Teamcity", "host", fallback = "http://dev.groundctl.com:8111")
path = config.get("Teamcity", "path", fallback = "/httpAuth/app/rest/builds")
url = host + path
common_locator = "branch:default:any,status:SUCCESS"
fields = "build(id,number,branchName,webUrl,buildType(id,name,projectName,projectId),comment(text),tags(tag(name)),finishDate,revisions(revision(version)),artifacts(file(name)))"
user = config.get("Teamcity", "user", fallback = "teambot")
password = config.get("Teamcity", "password")
auth = (user, password)
custom_headers = { "Accept": "application/json" }
verify = config.get("Teamcity", "cert", fallback = True)
if verify == "":
    verify = False

tags = ["qa", "release", "hold-my-beer-watch-this"]

slack_channel = config.get("Slack", "channel", fallback = "#qa")
slack_token = config.get("Slack", "token")
slack_prefix = config.get("Slack", "prefix", fallback = "@channel")

default_database_file = os.getenv("XDG_DATA_HOME", os.path.expanduser("~/.local/share")) + "/teambot.db"
database_file = config.get("Sqlite", "file", fallback = default_database_file)

class Build:
    def __init__(self, json):
        self.number = json["number"]

        try:
            self.comment = json["comment"]["text"].strip()
        except KeyError:
            self.comment = ""
        if self.comment == "":
            self.comment = None

        self.overview = json["webUrl"]

        self.branch_name = json["branchName"]
        head_prefix = "refs/heads/"
        if self.branch_name.startswith(head_prefix):
            self.branch_name = self.branch_name[len(head_prefix):]

        self.tags = []
        for i in json["tags"]["tag"]:
            self.tags.append(i["name"])

        revisions = json["revisions"]["revision"]
        if len(revisions) == 0:
            self.hash = "none"
        else:
            # We only have 1 source repo, so there should only be 1 revision
            self.hash = revisions[0]["version"]

        # Date of commit 96b9089cfe9dde8041fa9d1f09d2d9cb28f9f5ef
        s3_change_date = datetime(2017, 12, 12, 23, 00, 37, tzinfo=timezone.utc)
        build_date_str = json["finishDate"]
        try:
            build_date = datetime.strptime(build_date_str, "%Y%m%dT%H%M%S%z")
        except:
            build_date = None

        quote = urllib.parse.quote
        build_type = json["buildType"]
        if build_date is None or build_date >= s3_change_date:
            build_type_id = build_type["id"]
            project_id = build_type["projectId"]
            build_id = json["id"]
            # URL pattern from https://github.com/JetBrains/teamcity-s3-artifact-storage-plugin/blob/fae0f6679c1a58e676f60789df67b1f6eaf1227b/s3-artifact-storage-agent/src/main/java/jetbrains/buildServer/artifacts/s3/publish/S3ArtifactsPublisher.java#L245
            s3_prefix = "http://builds.groundctl.com/" + quote(project_id) + "/" + quote(build_type_id) + "/" + quote(str(build_id)) + "/"
        else:
            build_name = build_type["name"]
            project_name = build_type["projectName"]
            s3_prefix = "http://builds.groundctl.com/" + quote(project_name) + "::" + quote(build_name) + "/" + quote(self.number) + "/"

        self.win_installer = None
        self.mac_installer = None
        for i in json["artifacts"]["file"]:
            path = i["name"]
            if path.endswith("exe"):
                self.win_installer = s3_prefix + quote(path)
            elif path.endswith("dmg"):
                self.mac_installer = s3_prefix + quote(path)

    def comment_or(self, value):
        if self.comment:
            return self.comment
        return value

try:
    builds = dict()

    for tag in tags:
        tag_builds = requests.get(url, params = { "locator": common_locator + ",tags:" + tag , "fields": fields }, headers = custom_headers, auth = auth, verify = verify)
        tag_builds.raise_for_status()
        for build_json in tag_builds.json()["build"]:
            b = Build(build_json)
            builds[b.number] = b

    db = sqlite3.connect(database_file)
    cur = db.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS SlackPosts (buildNumber TEXT NOT NULL PRIMARY KEY, channel TEXT NOT NULL, timestamp TEXT NOT NULL, comment TEXT, qa BOOL NOT NULL, release BOOL NOT NULL)")

    for num in builds:
        build = builds[num]
        comment_or_none = build.comment_or("_No comment_")
        message = slack_prefix + ": <" + build.overview + "|GroundControl Launchpad " + num + "> "
        message += "(" + build.branch_name + ") (" + build.hash + ") "
        if "release" in build.tags:
            message += "(Release)"
        elif "qa" in build.tags:
            message += "(QA)"
        elif "hold-my-beer-watch-this" in build.tags:
            message += "(Hold my beer and watch this!)"
        message += ": " + html.escape(comment_or_none, False)
        if build.win_installer:
            message += "\nWindows: <" + build.win_installer + ">"
        if build.mac_installer:
            message += "\nMac OS X: <" + build.mac_installer + ">"
        rows = cur.execute("SELECT * FROM SlackPosts WHERE buildNumber=?", [num]).fetchall()
        if len(rows) != 0:
            # buildNumber is unique, so there can only be one entry
            entry = rows[0]
            saved_channel = entry[1]
            saved_timestamp = entry[2]
            saved_comment = entry[3]
            saved_qa = entry[4]
            saved_release = entry[5]
            if build.comment != saved_comment or ("qa" in build.tags) != saved_qa or ("release" in build.tags) != saved_release:
                response = requests.post("https://slack.com/api/chat.update", params = { "token": slack_token, "ts": saved_timestamp, "channel": saved_channel, "text": message, "parse": "none", "link_names": 1})
                response.raise_for_status()
                responseJSON = response.json()
                if not responseJSON["ok"]:
                    if responseJSON["error"] == "message_not_found":
                        # the message got deleted, remove it from our database
                        cur.execute("DELETE FROM SlackPosts WHERE buildNumber=?", [num])
                    else:
                        raise RuntimeError("Slack request failed:\n" + str(response.url) + "\n" + str(response.json()))
                else:
                    cur.execute("UPDATE SlackPosts SET comment=?, qa=?, release=? WHERE buildNumber=?", [build.comment, "qa" in build.tags, "release" in build.tags, num])
                db.commit()
        else:
            response = requests.post("https://slack.com/api/chat.postMessage", params = { "token": slack_token, "channel": slack_channel, "text": message, "parse": "none", "link_names": 1, "as_user": "true"})
            response.raise_for_status()
            if not response.json()["ok"]:
                raise RuntimeError("Slack request failed:\n" + str(response.url) + "\n" + str(response.json()))

            timestamp = response.json()["ts"]
            response_channel = response.json()["channel"]

            cur.execute("INSERT INTO SlackPosts VALUES (?, ?, ?, ?, ?, ?)", [num, response_channel, timestamp, build.comment, "qa" in build.tags, "release" in build.tags])
            db.commit()
except:
    response = requests.post("https://slack.com/api/chat.postMessage", params = { "token": slack_token, "channel": "@jeremyagost", "text": traceback.format_exc(), "parse": "full", "link_names": 0, "as_user": "false"})
    raise

